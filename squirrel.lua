
local S = mobs.intllib


-- Squirrel (textures and model taken from Petz mod by runs)

mobs:register_mob("mobs_animal:squirrel", {
	stepheight = 0.1,
	fly_in = "default:pine_needles",
	type = "animal",
	visual_size = {x=5, y=5},
	passive = false,
	attack_type = "dogfight",
	owner_loyal = true,
	attack_npcs = false,
	reach = 2,
	hp_min = 3,
	hp_max = 5,
	armor = 200,
	collisionbox = {-0.1875, -0.175, -0.1875, 0.1875, 0.1, 0.1875},
	visual = "mesh",
	mesh = "mobs_squirrel.b3d",
	textures = {
		{"mobs_squirrel.png"},
		{"mobs_squirrel2.png"},
		{"mobs_squirrel3.png"}
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_squirrel",
		damage = "mobs_squirrel_hurt"
	},
	walk_velocity = 1,
	run_velocity = 5,
	runaway = true,
	runaway_from = {"player", "mobs_animal:pumba"},
	randomly_turn = false,
	jump = true,
	jump_height = 6,
	follow = {"ethereal:pine_nuts", "farming:tigernuts", "farming:cabbage"},
	stay_near = {{"group:tree"}, 10},
	view_range = 8,
	water_damage = 0,
	lava_damage = 5,
	fall_damage = false,
	light_damage = 0,
	fear_height = 0,
	animation = {
		speed_normal = 25,
		stand_start = 26,
		stand_end = 45,
		stand1_start = 46,
		stand1_end = 58,
		die_start = 81,
		die_end = 93,
		die_loop = false,
		walk_start = 1,
		walk_end = 12,
		fly_start = 1,
		fly_end = 12,
		run_start = 13,
		run_end = 25
	},
	on_rightclick = function(self, clicker)

		if mobs:feed_tame(self, clicker, 8, true, true) then return end
		if mobs:protect(self, clicker) then return end
		if mobs:capture_mob(self, clicker, 30, 90, 0, false, nil) then return end
	end,
})

if minetest.get_modpath("ethereal") then
	spawn_on = {"ethereal:grove_dirt", "ethereal:prairie_dirt"}
end

mobs:spawn({
	name = "mobs_animal:squirrel",
	nodes = spawn_on,
	neighbors = {"group:grass"},
	min_light = 7,
	interval = 60,
	chance = 8000, -- 15000
	min_height = 5,
	max_height = 200,
	day_toggle = true,
})

mobs:register_egg("mobs_animal:squirrel", S("Squirrel"), "mobs_squirrel_inv.png")
